<?php
/**
 * ApiPromosClient
 */

namespace Apipromos\Sdk;

use Apipromos\Sdk\ApiPromosClient;
use Apipromos\Sdk\TraitFileToken;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Session;

class ApiPromosClientPassword extends ApiPromosClient
{
    use TraitFileToken; 

    public function generateTokenPassword($username, $password)
    { 
        $guzzle = new Client;

        $response = $guzzle->post($this->getUrlAccessToken(), [
            'form_params' => [
                'grant_type'    => 'password',
                'client_id'     => $this->getClientId(),
                'client_secret' => $this->getClientSecret(),
                'username'      => $username,
                'password'      => $password,
            ],
            'http_errors' => $this->getHttpErrors(),
        ]);

        return $response;
    }

    public function getToken()
    {
        return Session::get('API_PROMOS_CLIENT_PASSWORD_TOKEN');
    }
}
