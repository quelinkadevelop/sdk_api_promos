<?php

namespace Apipromos\Sdk;

use Apipromos\Sdk\ApiPromosClient;
use Apipromos\Sdk\ApiPromosClientPassword;

function api($config = null)
{
    return new ApiPromosClient($config);
}

function apiPassword($config = null)
{
    return new ApiPromosClientPassword($config);
}

function getApiDataResponse($response)
{
    return json_decode($response->getBody(), true)['data'];
}

function getApiMetaPaginationResponse($response)
{
    return json_decode($response->getBody(), true)['meta']['pagination'];
}

function getApiMessageResponse($response)
{
    return json_decode($response->getBody(), true)['message'];
}

function getApiErrorsResponse($response)
{
    return json_decode($response->getBody(), true)['errors'];
}
