<?php

namespace Apipromos\Sdk;

trait TraitFileToken
{

    public function getPathTokenFile()
    {
        return isset($this->pathTokenFile) ?: __DIR__ . "/tokenClient.txt";
    }

    public function saveToken($token)
    {
        $content = $token;
        $fp      = fopen($this->getPathTokenFile(), "wb");
        fwrite($fp, $content);
        fclose($fp);

        return $token;
    }

    public function readToken()
    {
        if (file_exists($this->getPathTokenFile())) {
            return file_get_contents($this->getPathTokenFile());
        }
        return null;
    }

    public function removeToken()
    {
        if (file_exists($this->getPathTokenFile())) {
            return unlink($this->getPathTokenFile());
        }

    }

}
