<?php

namespace Apipromos\Sdk;

use Apipromos\Sdk\Communication;
use Apipromos\Sdk\Coupon;
use Illuminate\Pagination\LengthAwarePaginator;

/**
 *
 */
class User extends AbstractModel
{
    protected $dates = [
        'birth_date',
        'created_at',
        'updated_at',
    ];

    public static function find($id)
    {
        $result = api()->get('users/' . $id);

        if ($result->getStatusCode() == 200) {
            $model = new self;
            $model->fill((array) getApiDataResponse($result));
            return $model;
        }

        return null;
    }

    public static function create($attributes = [])
    {
        $model = new self;
        $model->fill((array) $attributes);

        $result = api()->post('users/', $model->getAttributes());

        if ($result->getStatusCode() == 201) {
            return $model;
        }

        return null;
    }

    public static function createWithCoupons($userAttributes = [], $couponsAttributes = [])
    {
        $model = new self;
        $model->fill((array) $userAttributes);

        $result = api()->post('actions/register-user-with-valid-coupons/', ['user' => $userAttributes, 'coupons' => $couponsAttributes]);

        if ($result->getStatusCode() == 200) {
            return $model;
        }

        return null;
    }

    public function updatePartials($attributes = [])
    {

        $result = api()->patch('users/' . $this->id, $attributes);

        if ($result->getStatusCode() == 200) {
            $model = new self;
            $model->fill((array) getApiDataResponse($result));
            return $model;
        }

        return false;
    }

    public function update($attributes = [])
    {
        $result = api()->put('users/' . $this->id, $this->getAttributes());

        if ($result->getStatusCode() == 200) {
            $model = new self;
            $model->fill((array) getApiDataResponse($result));
            return $model;
        }

        return json_decode($result->getBody(), true);
    }

    public function delete()
    {
        $result = api()->delete('users/' . $this->id);

        if ($result->getStatusCode() == 200) {

            return $model;
        }

        return false;
    }

    public static function paginate($params = [], $config = [])
    {

        $collection = collect();

        $result = api()->get('users/', $params);

        if ($result->getStatusCode() == 200) {

            $data       = getApiDataResponse($result);
            $pagination = getApiMetaPaginationResponse($result);

            foreach ($data as $user) {
                $model = new self;
                $model->fill($user);
                $collection->push($model);
            }

            return new LengthAwarePaginator(
                $collection,
                $pagination['total'],
                $pagination['per_page'],
                $pagination['current_page'],
                [
                    'path'  => isset($config['path']) ? $config['path'] : request()->url(),
                    'query' => isset($config['query']) ? $config['query'] : request()->query(),
                ]
            );
        }

        return new LengthAwarePaginator($collection, 0, 1, 1);
    }

    public function coupons($params = [], $config = [])
    {

        $collection = collect();

        $result = api()->get('users/' . $this->id . '/coupons', $params);

        if ($result->getStatusCode() == 200) {

            $data       = getApiDataResponse($result);
            $pagination = getApiMetaPaginationResponse($result);

            foreach ($data as $value) {
                $model = new Coupon;
                $model->fill($value);
                $collection->push($model);
            }

            return new LengthAwarePaginator(
                $collection,
                $pagination['total'],
                $pagination['per_page'],
                $pagination['current_page'],
                [
                    'path'  => isset($config['path']) ? $config['path'] : request()->url(),
                    'query' => isset($config['query']) ? $config['query'] : request()->query(),
                ]
            );
        }

        return new LengthAwarePaginator($collection, 0, 1, 1);
    }

    public function promotions($params = [], $config = [])
    {

        $collection = collect();

        $result = api()->get('users/' . $this->id . '/promotions', $params);

        if ($result->getStatusCode() == 200) {

            $data       = getApiDataResponse($result);
            $pagination = getApiMetaPaginationResponse($result);

            foreach ($data as $value) {

                if (isset($value['coupons']['data'])) {

                    $collectionCouponsModels = collect();

                    if (count($value['coupons']['data']) > 0) {

                        foreach ($value['coupons']['data'] as $key => $couponValue) {
                            $newCoupon = new Coupon;
                            $newCoupon->fill($couponValue);
                            $collectionCouponsModels->push($newCoupon);
                        }
                    }

                    $value['coupons'] = $collectionCouponsModels;
                }

                $model = new Promotion;
                $model->fill($value);
                $collection->push($model);
            }

            return new LengthAwarePaginator(
                $collection,
                $pagination['total'],
                $pagination['per_page'],
                $pagination['current_page'],
                [
                    'path'  => isset($config['path']) ? $config['path'] : request()->url(),
                    'query' => isset($config['query']) ? $config['query'] : request()->query(),
                ]
            );
        }

        return new LengthAwarePaginator($collection, 0, 1, 1);
    }

    public function notifications($params = [], $config = [])
    {

        $collection = collect();

        $result = api()->get('users/' . $this->id . '/communications', $params);

        if ($result->getStatusCode() == 200) {

            $data       = getApiDataResponse($result);
            $pagination = getApiMetaPaginationResponse($result);

            foreach ($data as $value) {
                $model = new Communication;
                $model->fill($value);
                $collection->push($model);
            }

            return new LengthAwarePaginator(
                $collection,
                $pagination['total'],
                $pagination['per_page'],
                $pagination['current_page'],
                [
                    'path'  => isset($config['path']) ? $config['path'] : request()->url(),
                    'query' => isset($config['query']) ? $config['query'] : request()->query(),
                ]
            );
        }

        return new LengthAwarePaginator($collection, 0, 1, 1);
    }

    public function awards($params = [], $config = [])
    {

        $collection = collect();

        $result = api()->get('users/' . $this->id . '/awards', $params);

        if ($result->getStatusCode() == 200) {

            $data       = getApiDataResponse($result);
            $pagination = getApiMetaPaginationResponse($result);

            foreach ($data as $value) {
                $model = new Award;
                $model->fill($value);
                $collection->push($model);
            }

            return new LengthAwarePaginator(
                $collection,
                $pagination['total'],
                $pagination['per_page'],
                $pagination['current_page'],
                [
                    'path'  => isset($config['path']) ? $config['path'] : request()->url(),
                    'query' => isset($config['query']) ? $config['query'] : request()->query(),
                ]
            );
        }

        return new LengthAwarePaginator($collection, 0, 1, 1);
    }

    public static function getTokenResetPassword($email = '')
    {
        $postParams = ['email' => $email];
        $result     = api()->post('password/email', $postParams);

        if ($result->getStatusCode() == 200) {
            return json_decode($result->getBody()->getContents());
        }
        return null;
    }

    public static function resetPassword($postParams = [])
    {
        $result = api()->post('password/reset', $postParams);

        if ($result->getStatusCode() == 200) {
            return json_decode($result->getBody()->getContents());
        }
        return null;
    }

}
