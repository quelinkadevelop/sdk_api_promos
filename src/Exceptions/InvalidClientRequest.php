<?php

namespace Apipromos\Sdk\Exceptions;

use Exception;

class InvalidClientRequest extends Exception
{
    private $_request;
    private $_message;

    public function __construct($message, $code = 0, Exception $previous = null, $request = null)
    {

        $this->_request = $request;
        $this->_message = $message;

        parent::__construct($this->getMessageException(), $code, $previous);

    }
    public function getMessageException()
    {
        if (isset($this->getBody()['message'])) {
            return $this->_message . ' : ' . $this->getBody()['message'];
        }
        return $this->_message;

    }

    public function getRequest()
    {
        return $this->_request;
    }

    public function getBody()
    {
        return json_decode($this->_request->getBody(), true);
    }

    public function getErrors()
    {
        if (isset($this->getBody()['errors'])) {
            return json_decode($this->_request->getBody(), true)['errors'];
        }
        return $this->getBody();
    }
}
