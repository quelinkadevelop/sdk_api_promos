<?php

namespace Apipromos\Sdk;

use Apipromos\Sdk\User;
use Illuminate\Support\Facades\Session;

/**
 *
 */
class Auth extends User
{
    public function __construct()
    {

    }

    public static function user()
    {
        if (!Session::has('API_PROMOS_CURRENT_USER')) {
            return null;
        }
        $model = new User;
        $model->fill((array) Session::get('API_PROMOS_CURRENT_USER'));
        return $model;
    }

    public static function attempt($email, $password)
    {
        $result = apiPassword()->generateTokenPassword($email, $password);

        if ($result->getStatusCode() == 200) {

            $responseData = json_decode((string) $result->getBody(), true);
            Session::put('API_PROMOS_CLIENT_PASSWORD_TOKEN', $responseData['access_token']);

            $user                   = apiPassword()->get('me');
            $userData               = json_decode($user->getBody(), true)['data'];
            $userData['data_token'] = $responseData;

            Session::put('API_PROMOS_CURRENT_USER', $userData);

            return true;
        } else {
            Session::forget('API_PROMOS_CURRENT_USER');
            Session::forget('API_PROMOS_CLIENT_PASSWORD_TOKEN');
        }

        return false;
    }

    public static function logout()
    {
        apiPassword()->post('me/logout');
        Session::forget('API_PROMOS_CURRENT_USER');
        Session::forget('API_PROMOS_CLIENT_PASSWORD_TOKEN');
        return true;

    }

    public static function check()
    {
        if (Session::has('API_PROMOS_CURRENT_USER') && Session::has('API_PROMOS_CLIENT_PASSWORD_TOKEN')) {
            return true;
        }
        return false;
    }

}
